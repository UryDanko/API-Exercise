node {
    def appName = "cs"
    def k8sFilePath = "K8s.yaml"
    def projectName = "xxxxx"
    def dockerImage = "eu.gcr.io/${projectName}/${appName}:${image_tag}"
    def jdk = tool name: 'JDK8'
    env.JAVA_HOME = "${jdk}"
    def mvnHome = tool 'M3'
    env.PATH = "/opt/google-cloud-sdk/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"

    try {
        stage('Checkout code') {
            def scmVars = checkout scm
            commitHash = scmVars.GIT_COMMIT
        }

        stage('Configure') {
            sh "${mvnHome}/bin/mvn clean package -DdockerRepo=${dockerImage}"
            sh "docker push ${dockerImage}"
        }

        stage('Deploy') {
            def projectConfig = readProperties file: "./src/main/resources/application.properties"
            sh("sed -i.bak 's#xxxxxxxxxx#${dockerImage}#' ${k8sFilePath}")
            sh("sed -i.bak 's#SQL_INSTANCE_PlaceHolder#${projectConfig.gcp_sql_instance}#' ${k8sFilePath}")
            sh("kubectl apply -f ${k8sFilePath}")
        }
    } catch (e) {
        currentBuild.result = 'FAILURE'
        throw e
    }
}
