#!/bin/bash

docker build . -t ydanko/sandbox:latest
docker push ydanko/sandbox:latest

kubectl apply -f k8s.yaml