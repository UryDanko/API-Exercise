ARG BUILD_IMAGE=maven:3.5-jdk-8
ARG RUNTIME_IMAGE=openjdk:8-jre-alpine

# process dependencies #
FROM ${BUILD_IMAGE} as dependencies
COPY pom.xml ./
RUN mvn -B dependency:go-offline

# build app
FROM dependencies as build
COPY src ./src
RUN mvn -B clean package

# create final app docker image
FROM ${RUNTIME_IMAGE}
LABEL maintainer="UryDanko@gmail.com"
LABEL vendor="UryDanko"
RUN addgroup -g 1001 cs && adduser -u 1001 cs -G cs -D

ARG DEPENDENCY=/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

EXPOSE 8080
ENTRYPOINT ["java","-XX:+UnlockExperimentalVMOptions","-XX:+UseCGroupMemoryLimitForHeap","-XX:MaxRAMFraction=1","-XshowSettings:vm","-cp","app:app/lib/*","com.cs.assignment.AssignmentApplication"]
