DROP TABLE IF EXISTS `passengers`;
CREATE TABLE `passengers`
(
    `uuid`     varbinary(16) NOT NULL,
    `name`     varchar(255)  NOT NULL,
    `p_class`  int(11)       NOT NULL,
    `age`      int(11)       NOT NULL,
    `sex`      varchar(255)  NOT NULL,
    `siblings` int(11)       NOT NULL,
    `parents`  int(11)       NOT NULL,
    `fare`     float(11)     NOT NULL,
    `survived` bool          NOT NULL,
    PRIMARY KEY (`uuid`)
);