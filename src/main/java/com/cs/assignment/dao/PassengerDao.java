package com.cs.assignment.dao;

import com.cs.assignment.model.Passenger;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface PassengerDao extends CrudRepository<Passenger, UUID> {
}
