package com.cs.assignment;

import com.cs.assignment.service.PassengerService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

@SpringBootApplication
public class AssignmentApplication {

    @Autowired
    private PassengerService passengerService;

    public static void main(String[] args) {
        SpringApplication.run(AssignmentApplication.class, args);
    }

    @Bean
    InitializingBean sendDatabase() {
        return () -> {
            passengerService.parseCSV(new ClassPathResource("titanic.csv").getFile());
        };
    }
}


