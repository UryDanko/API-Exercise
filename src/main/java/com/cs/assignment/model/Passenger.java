package com.cs.assignment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "passengers")
public class Passenger {
    @Id
    private UUID uuid;
    private String name;
    private String sex;
    private boolean survived;
    @Column(name = "p_class")
    private int pClass;
    private float age;
    @Column(name = "siblings")
    private int siblingsOrSpousesAboard;
    @Column(name = "parents")
    private int parentsOrChildrenAboard;
    private float fare;

    public Passenger() {
    }

    public Passenger(UUID uuid,
                     String name,
                     String sex,
                     boolean survived,
                     int pClass,
                     float age,
                     int siblingsOrSpousesAboard,
                     int parentsOrChildrenAboard,
                     float fare) {
        this.uuid = uuid;
        this.name = name;
        this.sex = sex;
        this.survived = survived;
        this.pClass = pClass;
        this.age = age;
        this.siblingsOrSpousesAboard = siblingsOrSpousesAboard;
        this.parentsOrChildrenAboard = parentsOrChildrenAboard;
        this.fare = fare;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean isSurvived() {
        return survived;
    }

    public void setSurvived(boolean survived) {
        this.survived = survived;
    }

    public int getpClass() {
        return pClass;
    }

    public void setpClass(int pClass) {
        this.pClass = pClass;
    }

    public float getAge() {
        return age;
    }

    public void setAge(float age) {
        this.age = age;
    }

    public int getSiblingsOrSpousesAboard() {
        return siblingsOrSpousesAboard;
    }

    public void setSiblingsOrSpousesAboard(int siblingsOrSpousesAboard) {
        this.siblingsOrSpousesAboard = siblingsOrSpousesAboard;
    }

    public int getParentsOrChildrenAboard() {
        return parentsOrChildrenAboard;
    }

    public void setParentsOrChildrenAboard(int parentsOrChildrenAboard) {
        this.parentsOrChildrenAboard = parentsOrChildrenAboard;
    }

    public float getFare() {
        return fare;
    }

    public void setFare(float fare) {
        this.fare = fare;
    }

    @Override
    public String toString() {
        return "Passenger{" + name + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passenger passenger = (Passenger) o;

        if (survived != passenger.survived) return false;
        if (pClass != passenger.pClass) return false;
        if (age != passenger.age) return false;
        if (siblingsOrSpousesAboard != passenger.siblingsOrSpousesAboard) return false;
        if (parentsOrChildrenAboard != passenger.parentsOrChildrenAboard) return false;
        if (Float.compare(passenger.fare, fare) != 0) return false;
        if (!name.equals(passenger.name)) return false;
        return sex.equals(passenger.sex);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + sex.hashCode();
        result = 31 * result + (survived ? 1 : 0);
        result = 31 * result + pClass;
        result = 31 * result + (age != +0.0f ? Float.floatToIntBits(age) : 0);
        result = 31 * result + siblingsOrSpousesAboard;
        result = 31 * result + parentsOrChildrenAboard;
        result = 31 * result + (fare != +0.0f ? Float.floatToIntBits(fare) : 0);
        return result;
    }
}
