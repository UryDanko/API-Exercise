package com.cs.assignment.controller;

import com.cs.assignment.model.Passenger;
import com.cs.assignment.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController()
@RequestMapping("/people")
public class PassengerController {

    @Autowired
    private PassengerService passengerService;

    @GetMapping("/")
    public Iterable<Passenger> getPassengers() {
        return passengerService.getPassengers();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public UUID createPassenger(@RequestBody @Valid @NonNull Passenger passenger) {
        return passengerService.addPassenger(passenger);
    }

    @PutMapping(value = "/{uuid}",
            produces = "application/json; charset=utf-8",
            consumes = "application/json; charset=utf-8"
    )

    @ResponseStatus(HttpStatus.OK)
    public void updatePassenger(@PathVariable("uuid") UUID uuid, @RequestBody @Valid Passenger passenger) {
        passengerService.updatePassenger(uuid, passenger);
    }

    @GetMapping("/{uuid}")
    public Passenger getPassenger(@PathVariable("uuid") UUID uuid) {
        return passengerService.getPassenger(uuid);
    }

    @DeleteMapping("/{uuid}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("uuid") UUID uuid) {
        passengerService.deleteById(uuid);
    }


}
