package com.cs.assignment.service;

import com.cs.assignment.model.Passenger;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public interface PassengerService {

    Iterable<Passenger> getPassengers();

    UUID addPassenger(Passenger passenger);

    void updatePassenger(UUID uuid, Passenger passenger);

    Passenger getPassenger(UUID uuid);

    void deleteById(UUID uuid);

    void parseCSV(File file) throws IOException;
}
