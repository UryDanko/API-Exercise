package com.cs.assignment.service;

import com.cs.assignment.dao.PassengerDao;
import com.cs.assignment.model.Passenger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

@Service
class PassengerServiceDefaultImpl implements PassengerService {

    @Autowired
    private PassengerDao passengerDao;

    @Override
    public Iterable<Passenger> getPassengers() {
        return passengerDao.findAll();
    }

    @Override
    public UUID addPassenger(Passenger passenger) {
        passenger.setUuid(UUID.randomUUID());
        return passengerDao.save(passenger).getUuid();
    }

    @Override
    public void updatePassenger(UUID uuid, Passenger passenger) {
        passengerDao.findById(uuid).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + uuid));
        passenger.setUuid(uuid);
        passengerDao.save(passenger);
    }

    @Override
    public Passenger getPassenger(UUID uuid) {
        return passengerDao.findById(uuid).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + uuid));
    }

    @Override
    public void deleteById(UUID uuid) {
        passengerDao.findById(uuid).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + uuid));
        passengerDao.deleteById(uuid);
    }

    @Override
    public void parseCSV(File passengerCSVList) throws IOException {
        List<String> passengersCSV = Files.readAllLines(passengerCSVList.toPath());

        for (int i = 1; i < passengersCSV.size(); i++) {
            String[] values = passengersCSV.get(i).split(",");
            Passenger passenger = new Passenger(
                    UUID.randomUUID(),
                    values[2],
                    values[3],
                    values[0] == "1",
                    Integer.parseInt(values[1]),
                    Float.parseFloat(values[4]),
                    Integer.parseInt(values[5]),
                    Integer.parseInt(values[6]),
                    Float.parseFloat(values[7])
            );
            passengerDao.save(passenger);
        }
    }
}
